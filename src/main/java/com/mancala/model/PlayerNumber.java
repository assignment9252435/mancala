package com.mancala.model;

public enum PlayerNumber {
    ONE,
    TWO
}