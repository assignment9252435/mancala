package com.mancala.ui;

import com.mancala.model.Board;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestPrettyPrint {

    @Test
    void shouldPrettyPrintBoard() {
        Board board = Board.create();
        String view = PrettyPrint.board(board);
        assertThat(view).isEqualTo("""
                       Player Two
             | 06 | 06 | 06 | 06 | 06 | 06 |
        (00)                                 (00)
             | 06 | 06 | 06 | 06 | 06 | 06 |
                       Player One
        """, view);
    }

}
